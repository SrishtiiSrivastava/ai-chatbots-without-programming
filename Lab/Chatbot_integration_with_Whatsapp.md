## Lab : Watson Integration with chatbot
Objective for Exercise:
- How to integrate the basic chatbot with Twillio on your Whatsapp.
- How to deploying your Watson-powered chatbot to Whatsapp for your business.

WhatsApp uses your phone's cellular or Wi-Fi connection to facilitate messaging to nearly anyone on the planet for personal uses, also can help businesses and customers communicate with each other. You can automate the communication with your customers by deploying your Watson-powered chatbot to Whatsapp for your business. Even if you are not present, you will be able to reply to client inquiries quickly.

## Prerequisites:
1. Watson Assistant with the Flower Shop service Skill JSON loaded.
2. Twillio Signup

## Task 1. Add Flower Shop service  skill on Watson Assistant
1. On your Watson Assistant page, click on _**Create Assistant**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/1.png)

2. On the Create Assistant page, add a name to your assistant, like Flower Shop Assistant and click _**Create Assistant**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/2.2.png)

3. Once your assistant is created, click on _**Add dialog skill**_, to add a skill to your assistant.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/2.png)

4. Click on _**Upload skill**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/4.png)

5. Upload [Flower shop.json](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Flower-Shop-Skill-dialog.json) file after downloading it on your pc by click here to select a file.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/5.png)

6. After uploading, you will see dialog box of Flower Shop skill.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/7.png)
 
Now, you have successfully uploaded the Flower Shop skill.

## Task 2. Twilio Signup
We will be using Twilio's sandbox to show how this integration works to build and have a conversation with a chatbot through WhatsApp.

1. Click on [Twilio Signup link](https://www.twilio.com/try-twilio?promo=jO1067) and signup with your details.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task2/start%20your%20free%20trial.png)

2. Verify your email.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task2/verify_your_email.png)

3. Verify your phone number.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task2/verify%20your%20number.png)

4. Add configuration as suggested below and Click on _**Get started with Twilio**_ 

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task2/set%20configuration.png)




## Task 3. Integration with Twillio on your Whatsapp

1. On your [Watson Assistant page](https://eu-de.assistant.watson.cloud.ibm.com/crn%3Av1%3Abluemix%3Apublic%3Aconversation%3Aeu-de%3Aa%2F563ee29a43d1413bb281e9aa78cce1c8%3Ae9127ee5-e3f6-4572-8786-2bf01eeec0fa%3A%3A/home), click on _**Flower Shop Assistant**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/Click_on_Flower_shop_assistant.png)

2. Click on _**Add Integration**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/add%20integration.png)

3. In third party integration, Click on _**WatsApp with Twillio**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/Watsapp%20with%20twillio.png)

4. Click _**Create**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/Click%20create%20on%20twillio.png)

5. Once you setup your [Twilio account](https://www.twilio.com/login), Go to search bar and search for _**Console Dashboard**_. 

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/Console%20dashboard.png)

6. Copy **Account SID** and **Auth Token**.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/IDS.png)

7. Paste the copied Account SID and Auth Token in the [WhatsApp with Twilio page](https://eu-de.assistant.watson.cloud.ibm.com/crn%3Av1%3Abluemix%3Apublic%3Aconversation%3Aeu-de%3Aa%2F563ee29a43d1413bb281e9aa78cce1c8%3Ae9127ee5-e3f6-4572-8786-2bf01eeec0fa%3A%3A/assistants/69737913-c8ce-4810-8b0e-b227b8063924) of Watson Assistant and copy the **Whatsapp Webhook Integration link**. Please make a note of it as it will be used in coming steps.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/web%20hook.png)

8. Go to Twillio account. On the left pannel **Develop** , Under Messaging > Try it out > Click on _**Send Whatsapp message**_ and Tick on the check box that will popup to activate your Sandbox and Click on _**Confirm**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/sandbox%20check%20in.png)

9. In **Learn: Twilio Sandbox for WhatsApp** page, save the encircled number on your mobile phone and send the encircled code as a Whatsapp message to that number.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/send%20code.png)

10. After sending the code, your WhatsApp screen should look like this.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/send%20code%20on%20watsapp.png)

11. Then you will recieve a confirmation message on your Twillio account that your WhatsApp phone number is linked to your sandbox and click on _**Next: Send a One-Way Message**_. 

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/one%20way%20message.png)

12. Then, Click on _**Next: Two-Way Messaging**_. 

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/two%20way%20message.png)

13. Then, Click on _**Next: Configure your Sandbox**_ 

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/configure%20your%20sandbox.png)

14. Replace the copied **webhook integration link** that you have obtained earlier above from [WhatsApp with Twilio page](https://eu-de.assistant.watson.cloud.ibm.com/crn%3Av1%3Abluemix%3Apublic%3Aconversation%3Aeu-de%3Aa%2F563ee29a43d1413bb281e9aa78cce1c8%3Ae9127ee5-e3f6-4572-8786-2bf01eeec0fa%3A%3A/assistants/69737913-c8ce-4810-8b0e-b227b8063924) of Watson Assistant to Twilio Sandbox for WhatsApp.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/webhook%20paste.png)

15. Then scroll down the same page and Click _**Save**_.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/webhook%20paste-save.png)

14. Now your chat bot is completely integrated with whatsapp. Go ahead and test the chatbot you designated.

![](https://gitlab.com/Ratima_27/ai-chatbots-without-programming/-/raw/main/Images/Task3/Testing.png)


